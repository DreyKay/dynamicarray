#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "../dynamicArray.h"


int main()
{ 
    unsigned int* puint = NULL;
    int size = 0;

    srand(time(NULL)); //rand initialization
    size = rand() % 100; //returns random numbers between 0 100 at each call

    printf("Create array of %d unsigned ints\n", size);

    puint = dynamicArrayUInt(size); //creation of an unsigned int array

    for (int i = 0; i < size; i++)
    {
       puint[i] = rand() % 65535; //the random number will have as maximum value 65535 which is the maximum range of the unsigned data type int
    }
    
    for (int i = 0; i < size; i++) //A loop 'for' so that there is no comma at the last number obtained
    {
        if (i < size - 1)
        {
            printf("%u, ", puint[i]);
        }
        else
        {
            printf("%u\n", puint[i]);
        }
    }

    return 0;
}
