#include <stdlib.h>
#include "dynamicArray.h"

/*
 * Creates a array of char with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
char* dynamicArrayChar(unsigned int size) 
{
	return calloc(size, sizeof(char)); 
}

/*
 * Creates a array of unsigned char with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned char* dynamicArrayUChar(unsigned int size)
{
	return calloc(size, sizeof(char));
}

/*
 * Creates a array of short with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
short* dynamicArrayShort(unsigned int size)
{
	return calloc(size, sizeof(short));
}

/*
 * Creates a array of unsigned short with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned short* dynamicArrayUShort(unsigned int size)
{
	return calloc(size, sizeof(short));
}

/*
 * Creates a array of int with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
int* dynamicArrayInt(unsigned int size)
{
	return calloc(size, sizeof(int));
}

/*
 * Creates a array of unsigned int with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned int* dynamicArrayUInt(unsigned int size)
{
	return calloc(size,sizeof (int));
}

/*
 * Creates a array of long with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
long* dynamicArrayLong(unsigned int size)
{
	return calloc(size, sizeof(long));
}

/*
 * Creates a array of unsigned long with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned long* dynamicArrayULong(unsigned int size)
{
	return calloc(size, sizeof(long));
}

/*
 * Creates a array of float with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
float* dynamicArrayFloat(unsigned int size)
{
	return calloc(size, sizeof(float));
}

/*
 * Creates a array of double with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
double* dynamicArrayDouble(unsigned int size)
{
	return calloc(size, sizeof(double));
}

/*
 * Creates a array of long double with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
long double* dynamicArrayLDouble(unsigned int size)
{	
	return calloc(size, sizeof(long double));
}

