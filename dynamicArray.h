#ifndef DYNAMICARRAY_H_
#define DYNAMICARRAY_H_

/*
 * Creates a array of char with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
char* dynamicArrayChar(unsigned int size);

/*
 * Creates a array of unsigned char with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned char* dynamicArrayUChar(unsigned int size);

/*
 * Creates a array of short with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
short* dynamicArrayShort(unsigned int size);

/*
 * Creates a array of unsigned short with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned short* dynamicArrayUShort(unsigned int size);

/*
 * Creates a array of int with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
int* dynamicArrayInt(unsigned int size);

/*
 * Creates a array of unsigned int with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned int* dynamicArrayUInt(unsigned int size);

/*
 * Creates a array of long with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
long* dynamicArrayLong(unsigned int size);

/*
 * Creates a array of unsigned long with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
unsigned long* dynamicArrayULong(unsigned int size);

/*
 * Creates a array of float with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
float* dynamicArrayFloat(unsigned int size);

/*
 * Creates a array of double with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
double* dynamicArrayDouble(unsigned int size);

/*
 * Creates a array of long double with defined size.
 *
 * Parameters:
 * 'size': The wanted size of the array.
 *
 * Returned value:
 * The pointer of the created array.
 * NULL if no array was created.
 */
long double* dynamicArrayLDouble(unsigned int size);

#endif /* DYNAMICARRAY_H_ */
